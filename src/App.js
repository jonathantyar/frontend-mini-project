import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import PokemonListPage from './app/pages/pokemonListPage/pokemonListPage';
import PokemonDetailPage from './app/pages/pokemonDetailPage/pokemonDetailPage';
import MyPokemonPage from './app/pages/myPokemonPage/myPokemonPage';

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/my-pokemon' component={MyPokemonPage} />
        <Route path='/pokemon/:id' component={PokemonDetailPage} />
        <Route path='/' component={PokemonListPage} />
      </Switch>
    </Router>
  );
}

export default App;
