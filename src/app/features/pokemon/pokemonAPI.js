import axios from 'axios';

export function fetchPokemon(params) {
  return axios.get(params);
}
