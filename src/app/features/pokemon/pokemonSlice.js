import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchPokemon } from './pokemonAPI';

const initialState = {
  data: false,
  status: 'idle',
};

export const fetchAsyncPokemon = createAsyncThunk(
  'pokemon/fetchPokemon',
  async (params) => {
    const response = await fetchPokemon(params);
    return response.data;
  }
);

export const fetchMoreAsyncPokemon = createAsyncThunk(
  'pokemon/fetchMorePokemon',
  async (params) => {
    const response = await fetchPokemon(params);
    return response.data;
  }
);

export const pokemonSlice = createSlice({
  name: 'pokemonList',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchAsyncPokemon.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchAsyncPokemon.fulfilled, (state, action) => {
        state.status = 'idle';
        state.data = action.payload;
      })
      .addCase(fetchMoreAsyncPokemon.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchMoreAsyncPokemon.fulfilled, (state, action) => {
        state.status = 'idle';
        const dataBefore = state.data.results;
        state.data = action.payload;
        state.data.results = [...dataBefore, ...state.data.results];
      });
  },
});

export const selectPokemon = (state) => state.pokemon;

export default pokemonSlice.reducer;
