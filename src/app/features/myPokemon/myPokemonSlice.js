import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchMyPokemon } from './myPokemonAPI';

const initialState = {
  data: false,
  status: 'idle',
};

export const fetchAsyncMyPokemon = createAsyncThunk(
  'pokemon/fetchMyPokemon',
  async (params) => {
    const response = await fetchMyPokemon(params);
    return response.data;
  }
);

export const myPokemonSlice = createSlice({
  name: 'myPokemon',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchAsyncMyPokemon.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchAsyncMyPokemon.fulfilled, (state, action) => {
        state.status = 'idle';
        state.data = action.payload;
      })
  },
});

export default myPokemonSlice.reducer;
