import axios from 'axios';

export function fetchMyPokemon(params) {
  return axios.get(params);
}
