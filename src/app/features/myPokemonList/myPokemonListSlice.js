import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchMyPokemonList } from './myPokemonListAPI';
import { BACKEND_API } from '../../constants';

const initialState = {
  data: false,
  status: 'idle',
};

export const fetchAsyncMyPokemonList = createAsyncThunk(
  'pokemon/fetchMyPokemonList',
  async () => {
    const response = await fetchMyPokemonList(BACKEND_API);
    return response.data;
  }
);

export const myPokemonListSlice = createSlice({
  name: 'myPokemonList',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchAsyncMyPokemonList.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchAsyncMyPokemonList.fulfilled, (state, action) => {
        state.status = 'idle';
        state.data = action.payload;
      })
  },
});

export default myPokemonListSlice.reducer;
