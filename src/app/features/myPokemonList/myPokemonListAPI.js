import axios from 'axios';

export function fetchMyPokemonList(params) {
  return axios.get(params);
}
