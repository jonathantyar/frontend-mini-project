import React from 'react';

import './modal.css';

const Modal = (props) => {
    return (
        <div className="modal" onClick={() => props.close()}>
            <div className="content">
                <h2>{props.text}</h2>
            </div>
        </div>
    );
};

export default Modal;