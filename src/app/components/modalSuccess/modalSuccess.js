import React, { useState } from 'react';

import './modalSuccess.css';

const ModalSuccess = (props) => {
    const [text, setText] = useState('');

    const handleChange = (e) => {
        setText(e.target.value);
    };

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            props.save(text);
            props.close();
        }
    }

    return (
        <div className="modal">
            <div className="content">
                <h2>Good Catch! Now give it a nickname!</h2>
                <input
                    type="text"
                    className="nickname"
                    value={text}
                    onChange={(e) => handleChange(e)}
                    onKeyDown={(e) => handleKeyDown(e)}
                />
                <button className="save" onClick={() => {
                    props.save(text);
                    props.close();
                }}>Save</button>
            </div>
        </div>
    );
};

export default ModalSuccess;