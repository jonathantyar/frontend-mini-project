import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import ModalDelete from '../modalDelete/modalDelete';
import './pokemon.css';

const Pokemon = (props) => {
    const [Pokemon, setPokemon] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const history = useHistory();

    useEffect(() => {
        if (!Pokemon) {
            axios.get(props.url).then((response) => {
                setPokemon(response.data);
            })
        }
    }, [Pokemon, props.url])

    const handleClick = () => {
        if (props.myList) {
            setModalDelete(true);
            return;
        }

        history.push({
            pathname: `/pokemon/${Pokemon && Pokemon.id}`,
        })
    };

    const closeModal = () => {
        console.log('close modal');
        setModalDelete(false);
    }

    return (
        <div className="pokemon" >
            { Pokemon && (
                <div onClick={() => handleClick()}>
                    <img src={Pokemon.sprites.other.dream_world.front_default} alt={Pokemon.name} />
                    <h4>{Pokemon.name}</h4>
                </div>
            )}
            { modalDelete ? <ModalDelete text={`You will release ${Pokemon.name}`} close={() => setModalDelete(false)} delete={() => props.release()} /> : ''}
        </div >
    );
};

export default Pokemon;