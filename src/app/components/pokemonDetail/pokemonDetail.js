import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import './pokemonDetail.css';

const PokemonDetail = (props) => {
    const [Pokemon, setPokemon] = useState(false);

    useEffect(() => {
        if (!Pokemon) {
            axios.get(props.url).then((response) => {
                setPokemon(response.data);
            })
        }
    }, [Pokemon, props.url])

    console.log(Pokemon);
    console.log(props);

    return (
        <div className="pokemon-detail">
            {Pokemon && (
                <div>
                    <img src={Pokemon.sprites.other.dream_world.front_default} alt={Pokemon.name} className={`${props.catching ? 'wobble-hor-bottom' : ''}`} />
                    <h2>{Pokemon.name}</h2>
                    <div className="types">
                        {Pokemon.types.length && Pokemon.types.map((type) => (
                            <span key={type.type.name} className={type.type.name}>{type.type.name}</span>
                        ))}
                    </div>
                    <div className="moves">
                        {Pokemon.abilities.length && Pokemon.abilities.map((ability) => (
                            <span key={ability.ability.name} className={ability.ability.name}>{ability.ability.name}</span>
                        ))}
                    </div>
                    <div className="preview">
                        <img alt="back_default" src={Pokemon.sprites.back_default} />
                        <img alt="back_shiny" src={Pokemon.sprites.back_shiny} />
                        <img alt="front_default" src={Pokemon.sprites.front_default} />
                        <img alt="front_shiny" src={Pokemon.sprites.front_shiny} />
                    </div>
                </div>
            )}
        </div>
    );
};

export default PokemonDetail;