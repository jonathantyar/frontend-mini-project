import React from 'react';

import './modalDelete.css';

const ModalDelete = (props) => {
    return (
        <div className="modal" onClick={() => props.close()}>
            <div className="content">
                <h2>{props.text}</h2>
                <button onClick={() => props.delete()}>
                    Good Bye!
                </button>
            </div>
        </div>
    );
};

export default ModalDelete;