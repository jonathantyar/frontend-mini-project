import { configureStore } from '@reduxjs/toolkit';
import pokemonReducer from './features/pokemon/pokemonSlice';
import myPokemonReducer from './features/myPokemon/myPokemonSlice';
import myPokemonListReducer from './features/myPokemonList/myPokemonListSlice';

export const store = configureStore({
  reducer: {
    pokemon: pokemonReducer,
    myPokemon: myPokemonReducer,
    myPokemonList: myPokemonListReducer,
  },
});
