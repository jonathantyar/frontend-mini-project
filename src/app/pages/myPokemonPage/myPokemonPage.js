import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { fetchAsyncMyPokemonList } from '../../features/myPokemonList/myPokemonListSlice';

import Pokemon from '../../components/pokemon/pokemon';
import Back from '../../../assets/back.png';

import './myPokemonPage.css';
import axios from 'axios';
import { BACKEND_API } from '../../constants';

const MyPokemonPage = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const myPokemonList = useSelector(state => state.myPokemonList);

    useEffect(() => {
        dispatch(fetchAsyncMyPokemonList());
    }, [dispatch])

    const releasePokemon = async (id) => {
        await axios.delete(BACKEND_API + id).then((response) => {
            console.log(response);
            dispatch(fetchAsyncMyPokemonList());
        }).catch((err) => {
            console.log(err);
        })
    }

    return (
        <div className="taman">
            {myPokemonList.data && myPokemonList.data.data.map((pokemon) => (
                <Pokemon key={pokemon._id} url={pokemon.url} myList={true} release={() => releasePokemon(pokemon._id)} />
            ))}

            <div className="button" style={{ position: 'fixed' }}>
                <img alt="back" src={Back} onClick={() => history.goBack()} />
            </div>
        </div>
    );
};

export default MyPokemonPage;
