import React, { useState } from 'react';
import axios from 'axios';

import { BACKEND_API, POKE_API } from '../../constants';
import PokemonDetail from '../../components/pokemonDetail/pokemonDetail';
import Modal from '../../components/modal/modal';
import ModalSuccess from '../../components/modalSuccess/modalSuccess';

import './pokemonDetailPage.css';
import Chest from '../../../assets/chest.png';
import Pokeball from '../../../assets/pokeball.png';
import Back from '../../../assets/back.png';
import { useHistory } from 'react-router-dom';

const PokemonDetailPage = ({ match }) => {
    const history = useHistory();
    const [catching, setCatching] = useState(false);
    const [modalFail, setModalFail] = useState(false);
    const [modalAlert, setModalAlert] = useState(false);
    const [modalSuccess, setModalSuccess] = useState(false);

    const catchPokemon = () => {
        if (!catching) {
            setCatching(true);
            setTimeout(function () {
                const random_boolean = Math.random() < 0.5;
                if (random_boolean) {
                    setModalSuccess(true);
                } else {
                    setModalFail(true);
                }
                setCatching(false);
            }, 2000);
        }
    };

    const savePokemon = async (value) => {
        const data = {
            name: value,
            url: POKE_API + match.params.id
        }

        await axios.post(BACKEND_API, data)
            .then(function (response) {
                setModalAlert(true);
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    return (
        <div className="taman">
            {modalFail ? <Modal text="Failed to Catch the pokemon! Try Again!" close={() => setModalFail(false)} /> : ''}
            {modalAlert ? <Modal text="Gotcha pokemon was saved to your chest!" close={() => setModalAlert(false)} /> : ''}
            {modalSuccess ? <ModalSuccess close={() => setModalSuccess(false)} save={savePokemon} /> : ''}
            <PokemonDetail url={`${POKE_API}${match.params.id}`} catching={catching} />

            <div className="button">
                <img alt="back" src={Back} onClick={() => history.goBack()} />
                <img alt="pokeball" src={Pokeball} onClick={() => catchPokemon()} />
                <img alt="chest" src={Chest} onClick={() => history.push('/my-pokemon')} />
            </div>
        </div>
    );
};

export default PokemonDetailPage;