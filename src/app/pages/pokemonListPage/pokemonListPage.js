import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroll-component';

import { fetchAsyncPokemon, fetchMoreAsyncPokemon } from '../../features/pokemon/pokemonSlice';
import Pokemon from '../../components/pokemon/pokemon';
import { POKE_API } from '../../constants';

import Chest from '../../../assets/chest.png';
import Back from '../../../assets/back.png';
import './pokemonListPage.css';

const PokemonListPage = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const pokemonList = useSelector(state => state.pokemon);

    useEffect(() => {
        dispatch(fetchAsyncPokemon(POKE_API));
    }, [dispatch])

    console.log(pokemonList.data && pokemonList.data.results);

    const fetchMoreData = () => {
        dispatch(fetchMoreAsyncPokemon(pokemonList.data.next));
    };

    return (
        <div>
            <InfiniteScroll
                dataLength={pokemonList.data && pokemonList.data.results.length}
                next={fetchMoreData}
                hasMore={true}
                loader={<h4>Loading...</h4>}
                className="taman"
            >
                {pokemonList.data && pokemonList.data.results.map((pokemon) => (
                    <Pokemon key={pokemon.name} url={pokemon.url} />
                ))}
            </InfiniteScroll>

            <div className="button" style={{ position: 'fixed' }}>
                <img alt="back" src={Back} onClick={() => history.goBack()} />
                <img alt="chest" src={Chest} onClick={() => history.push('/my-pokemon')} />
            </div>
        </div>
    );
};

export default PokemonListPage;